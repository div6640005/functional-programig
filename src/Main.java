import java.util.List;
import java.util.Scanner;
import java.util.stream.IntStream;

public class Main {
    static List<Integer> list = List.of(3, 4, 5, 6, 7, 8, 9);

    public static void main(String[] args) {
        System.out.println(list);
        maxNumber();
        minNumber();
        sumNumber();
        squareNumber();
        factorialNumber();
        simpleNumber();
    }

    public static void maxNumber() {
        int f = list.stream()
                .mapToInt(v -> v)
                .max()
                .getAsInt();
        System.out.println("Max Number: " + f);
    }

    public static void minNumber() {
        List<Integer> list = List.of(3, 4, 5, 6, 7, 8, 9);
        int f = list.stream()
                .mapToInt(v -> v)
                .min()
                .getAsInt();
        System.out.println("Min Number: " + f);
    }

    public static void sumNumber() {
        int f = list.stream()
                .mapToInt(v -> v)
                .sum();
        System.out.println("Sum Number: " + f);
    }

    public static void squareNumber() {
        List<Integer> result = list.stream()
                .map(x -> x * x)
                .toList();
        System.out.println("Square List: " + result);
    }

    public static void factorialNumber() {
        List<Integer> result = list.stream()
                .map(x -> IntStream.rangeClosed(1, x).reduce(1, (z, y) -> z * y))
                .toList();
        System.out.println("Square List: " + result);
    }

    public static void simpleNumber() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter Number: ");
        int sc = scanner.nextInt();

        List<Integer> list1 = IntStream.range(2, sc)
                .filter(x -> x % 2 != 0)
                .filter(x -> IntStream.rangeClosed(3, (int) Math.sqrt(x))
                        .filter(t -> t % 2 != 0)
                        .noneMatch(n -> x % n == 0))
                .boxed()
                .toList();
        System.out.println(list1);

    }


}